phlmesh-build
=============
![License: MIT](http://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)

This is a docker container for the [OpenWRT](https://openwrt.org/)
[buildroot](http://wiki.openwrt.org/doc/howto/buildroot.exigence).

Because the build system requires that its command are not executed by root,
the user `lede` was created. The buildroot can be found in
`/home/lede/lede`.

To build the docker container, run:
```sh
docker build -t phlmesh-buildroot .
```

To build the image execute the following command:
```sh
docker run -t -i phlmesh-buildroot -v /home/youruser/phlmesh-build/:/mnt/ sudo -iu lede /mnt/build.sh
```

More information on how to use this buildroot can be found on the
[OpenWRT wiki](http://wiki.openwrt.org/doc/howto/build).
