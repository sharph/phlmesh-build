#!/bin/sh

git clone git://git.lede-project.org/source.git lede

cd lede
git checkout v17.01.4
./scripts/feeds update -a
./scripts/feeds install -a
cp -Rv /mnt/nano-m-files files
cp -v /mnt/nano-m .config
cd ..
