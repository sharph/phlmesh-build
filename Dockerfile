FROM ubuntu:16.04

RUN apt-get update &&\
    apt-get install -y git-core subversion sudo build-essential gcc-multilib \
                       libncurses5-dev zlib1g-dev gawk flex gettext wget unzip python \
                       libssl-dev &&\
    apt-get clean &&\
    useradd -m phlmesh &&\
    echo 'phlmesh ALL=NOPASSWD: ALL' > /etc/sudoers.d/phlmesh
